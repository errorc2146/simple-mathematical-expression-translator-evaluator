#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "token.h"
#include "lex.h"

#include "parseToPostfix.h"

int main(int argc, char** argv)
{
    if (argc <= 1)
    {
        printf("Please give me an expression to evaluate!\n");
        return 1;
    }
    
    token* stream = lex(argc, argv);
    char* postfix;
    int errorcode;
    if (!(errorcode = parseToPostfix(stream, &postfix)))
    {
        printf("parseToPostfix returned %d\n", errorcode);
        return -1;
    }

    printf("%s= %d\n", postfix, evaluatePostfix(postfix));
    
    free(stream);
    free(postfix);
    
    return 0;
}
