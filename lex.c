#include "lex.h"

token** lex(int argc, char** argv)
{
    //combine command line arguments into spaceless string
    int inSize = 0;
    for (int i = 1; i < argc; i++)
        inSize += sizeof(argv[i]);
    
    char *in = malloc(inSize);
    in[0] = '\0';
    for (int i = 1; i < argc; i++)
        strcat(in, argv[i]);
    
    int inlength = strlen(in);
    token** out = malloc((inlength + 1) * sizeof(token*)); //worst case every character needs its own token, plus the EOS
    int tpoint = 0;
    int i = 0;
    while (i < inlength)
    {
        token* new = malloc(sizeof(token));
        
        if (isdigit(in[i]))
        {
            int value = in[i++] - '0';
            while (isdigit(in[i]))
            {
                value *= 10;
                value += in[i++] - '0';
            }
            
            new->type = INT;
            new->value.intval = value;
        }
        
        else 
        {
            switch(in[i])
            {
                case '+': case '-': case 'x':
                case '*': case '/': case '%':
                    new->type = OP;
                    
                    if (in[i] == 'x') 
                    {
                        new->value.charval = '*';
                        i++;
                    }
                    else new->value.charval = in[i++];
                    break;
                
                case '(': case 'l':
                    new->type = LPAR;
                    i++;
                    break;
                    
                case ')': case 'r':
                    new->type = RPAR;
                    i++;
                    break;
                    
                default:
                    printf("Character '%c' at position %d doesn't fit a recognised token!\n", in[i], i);
                    new->type = UNKNOWN;
                    new->value.charval = in[i];
                    i++;
            }
        }

        out[tpoint++] = new;
    }
    
    free(in);
    
    token* end = malloc(sizeof(token));
    end->type = EOS;
    out[tpoint++] = end;
    return out;
}