#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>

#include "parseToPostfix.h"

#define TRUE 1
#define FALSE 0
#define SUCCESS 1
#define ERROR 0
#define LOOKAHEAD infix[fp]

token** infix;
int fp;

char *output;
int op;

void addc(char append);
void addi(int append);

int expr();
int expr2();

int term();
int term2();

int factor();

int match(int hope);

int parseToPostfix(token** in, char** outstream)
{
    int chars = 1, iterator;
    int type;
    
    for(iterator = 0; (type = in[iterator]->type) != EOS; iterator++)
    {
        if (type == OP || type == LPAR || type == RPAR)
            chars += 2;
        
        else if (type == INT)
            chars += (int)log10(in[iterator]->value.intval) + 2;
        
        else return ERROR;
    }
    output = *outstream = malloc(sizeof(char) * chars);
    
    output[0] = '\0';
    op = 0;
    
    infix = in;
    fp = 0;
    
    if (expr() == ERROR) 
    {
        printf("Could not parse to a postfix expression.\n");
        return ERROR;
    }
    
    return SUCCESS;
}

void addc(char append)
{
    output[op++] = append; 
    output[op++] = ' ';
    output[op] = '\0';
}

void addi(int append)
{
    int log = (int)log10(append);
    char* appendme = malloc (sizeof(char) * (log + 3));
    appendme[0] =  '\0';
    sprintf(appendme, "%d ", append);
    strcat(output, appendme);
    op += log + 2;
    free(appendme);
}

int match(int hope)
{
    if (LOOKAHEAD->type == hope)
    {
        fp++;
        return SUCCESS;
    }
    
    else
    {
        if (LOOKAHEAD->type == UNKNOWN) printf("Unknown symbol '%c'\n", LOOKAHEAD->value.charval);
        else printf("Expected type %d, got type %d\n", hope, LOOKAHEAD->type);
        return ERROR;
    }
}

int expr()
{
    if (term() == ERROR || expr2() == ERROR)
        return ERROR;
    else return SUCCESS;
}

int expr2()
{
    if(LOOKAHEAD->type == OP && LOOKAHEAD->value.charval == '+' || LOOKAHEAD->value.charval == '-')
    {
        char value = LOOKAHEAD->value.charval;
        match(LOOKAHEAD->type);
        if (term() == ERROR) return ERROR;
        addc(value);
        return expr2();
    } //otherwise assume empty string
    else return SUCCESS; 
}

int term()
{
    if (factor() == ERROR || term2() == ERROR)
        return ERROR;
    else return SUCCESS;
}

int term2()
{
    if(LOOKAHEAD->type == OP && LOOKAHEAD->value.charval == '*' || LOOKAHEAD->value.charval == '/')
    {
        char value = LOOKAHEAD->value.charval;
        match(OP);
        if (factor() == ERROR) return ERROR;
        addc(value);
        return term2();
    }
    else return SUCCESS;
}

int factor()
{
    if (LOOKAHEAD->type == INT)
    {
        addi(LOOKAHEAD->value.intval);
        match(INT);
        return SUCCESS;
    }
        
    else if (LOOKAHEAD->type == LPAR)
    {
        match(LPAR);
        if (expr() == ERROR) return ERROR;
        return match(RPAR);
    }
    
    else 
    {
        printf("Error: expected an integer or ( at character %d, found '%c'\n", fp, LOOKAHEAD->value.charval);
        return ERROR;
    }
}