#include <stdlib.h>
#include "evaluatePostfix.h"

#define SUCCESS 1
#define ERROR -1

typedef struct
{
    int sp;
    int maxbound;
    int* array;
} intStack;

void initStack(intStack* new, int size)
{
    new->array = malloc(sizeof(int) * size);
    new->maxbound = size;
    new->sp = 0;
}

void freeStack(intStack* finished)
{
    free(finished->array);
    free(finished);
}

int push(intStack* subject, int pushme)
{    
    if (subject->sp == subject->maxbound)
    {
        printf("Tried to push to a full stack!\n");
        return ERROR;
    }
    
    subject->array[subject->sp++] = pushme;
    return SUCCESS;
}

int pop(intStack* subject)
{
    if (subject->sp <= 0)
    {
        printf("Tried to pop an empty stack!\n");
        return ERROR;
    }
    
    return subject->array[--subject->sp];
}

int evaluatePostfix(char* in)
{
    if (in == ERROR) return ERROR;
    
    intStack* pile = malloc(sizeof(intStack));
    initStack(pile, 100);
    
    int fp = 0;
    
    while (in[fp] != '\0')
    {
        if (isdigit(in[fp]))
        {
            int value = in[fp++] - '0';
            while (isdigit(in[fp]))
            {
                value *= 10;
                value += in[fp++] - '0';
            }
            push(pile, value);
        }
        
        else
        {
            int im, im2;
            switch(in[fp])
            {
                case ' ':
                    break;
                case '+':
                    im = pop(pile);
                    im += pop(pile);
                    push(pile, im);
                    break;
                case '*':
                    im = pop(pile);
                    im *= pop(pile);
                    push(pile, im);
                    break;
                case '-':
                    im = pop(pile);
                    im2 = pop(pile);
                    push(pile, im2 - im);
                    break;
                case '/':
                    im = pop(pile);
                    im2 = pop(pile);
                    push(pile, im2 / im);
                    break;
                default:
                    printf("Unrecognised character '%c'\n", in[fp]);
                    break;
            }
            fp++;
        }
    }
    
    int result = pop(pile);
    freeStack(pile);
    return result;
}