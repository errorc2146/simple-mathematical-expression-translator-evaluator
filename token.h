#ifndef TOKEN_H
#define TOKEN_H

#define UNKNOWN 0
#define EOS 1
#define INT 2
#define OP 3
#define LPAR 4
#define RPAR 5

typedef struct
{
    int type;
    union
    {
        int intval;
        char charval;
    } value;
} token;

#endif