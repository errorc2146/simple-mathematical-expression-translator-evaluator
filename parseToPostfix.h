#ifndef PARSE_POSTFIX
#define PARSE_POSTFIX

#include "token.h"

int parseToPostfix(token** in, char** output);

#endif