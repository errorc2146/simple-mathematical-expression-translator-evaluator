#ifndef LEX_H
#define LEX_H

#include "token.h"

token** lex(int argc, char** argv);

#endif